/*
 * Sa se construiasca si sa se afiseze pentru fiecare nod din graf lista de
 * adiacenta definita dinamic.
 */

#include <iostream>
#include <fstream>
#include <vector>

using namespace std;

ifstream in("date.txt");

int main()
{
    int nr_noduri, val;
    in>>nr_noduri;

    for (int i=0; i<nr_noduri; i++) {
        cout<<"Nodul "<<(i+1)<<": ";

        for (int j=0; j<nr_noduri; j++) {
            in>>val;

            if (val) {
                cout<<(j+1)<<" ";
            }
        }

        cout<<endl;
    }

    return 0;
}
