/*
 * Sa se verifice daca o secventa de noduri citita
 * de la tastatura (incheiata cu -1) formeaza un lant in graf.
 */

#include <iostream>
#include <fstream>

using namespace std;

ifstream in("date.txt");

int main()
{
    int n;
    in>>n;

    bool A[n][n];

    for (int i=0; i<n; i++)
        for (int j=0; j<n; j++)
            in>>A[i][j];

    cout<<"Dati secventa de noduri pentru a detecta un lant, -1 pentru a finaliza:\n";

    int a, b;

    cin>>a;

    if (a > n || a == -1)
        return 0;

    while (1) {
        cin>>b;

        if (b == -1)
            break;

        if (b > n || !A[a-1][b-1]) {
            cout<<"Secventa de noduri nu formeaza un lant."<<endl;
            return 0;
        }

        a=b;
    }

    cout<<"Secventa de noduri formeaza un lant."<<endl;

    return 0;
}
