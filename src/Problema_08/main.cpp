/*
 * Scrieti un program care sa verifice daca un graf este regulat.
 */
#include <iostream>
#include <fstream>

using namespace std;

ifstream in("date.txt");

int main()
{
    int n;
    bool val, regulat=true;
    int ult_grad = -1;
    in>>n;

    for (int i=0; i<n; i++) {
        int grad = 0;

        for (int j=0; j<n; j++) {
            in>>val;

            if (val && i != j)
                grad++;
        }

        if (ult_grad != -1 && grad != ult_grad) {
            regulat=false;
            break;
        }

        ult_grad = grad;
    }

    if (regulat) {
        cout<<"Graful este regulat."<<endl;
    } else {
        cout<<"Graful nu este regulat."<<endl;
    }

    return 0;
}
