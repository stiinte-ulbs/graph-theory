/*
 * Scrieti un program care determina cate noduri sunt izolate in graf si afiseaza toate aceste noduri.
 */

#include <iostream>
#include <fstream>
#include <vector>

using namespace std;

ifstream in("date.txt");

int main()
{
    int nr_noduri;
    in>>nr_noduri;

    vector<int> noduri;

    for (int i=0; i<nr_noduri; i++) {
        int val;
        bool izolat = true;

        for (int j=0; j<nr_noduri; j++) {
            in>>val;

            if (val)
                izolat = false;
        }

        if (izolat)
            noduri.push_back(i + 1);
    }

    if (noduri.size() > 0) {
        cout<<"Noduri izolate: ";
        for (int val : noduri)
            cout<<val<<" ";

        cout<<endl;
    } else {
        cout<<"Niciun nod izolat.";
    }

    return 0;
}
