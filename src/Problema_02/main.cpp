/*
 * Se considera un graf neorientat exprimat prin intermediul unei matrici de
 * adiacenta (analog cu cel din problema anterioara):
 *
 * Scrieti un program care calculeaza gradul fiecarui nod din graf, sorteaza
 * descrescator si afiseaza nodurile si gradele aferente.
 */

#include <iostream>
#include <fstream>
#include <list>

using namespace std;

ifstream in("date.txt");

struct nod {
    int indice;
    int grad;
};

bool compara_nod(const nod &a, const nod &b) {
    return a.grad > b.grad;
}

int main()
{
    int nr_noduri;
    in>>nr_noduri;

    list<nod> noduri;

    for (int i=0; i<nr_noduri; i++) {
        int val, grad = 0;

        for (int j=0; j<nr_noduri; j++) {
            in>>val;

            if (val)
                grad++;
        }

        noduri.push_back({ i+1, grad });
    }

    noduri.sort(compara_nod);

    for (nod el : noduri) {
        cout<<"Nodul "<<el.indice<<" are gradul "<<el.grad<<endl;
    }

    return 0;
}
