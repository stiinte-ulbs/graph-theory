/*
 * Scrieti un program care sa verifice daca un graf este conex. Daca
 * graful nu este conex sa se determine (si afiseze) componentele conexe ale grafului.
 */

#include <iostream>
#include <fstream>

using namespace std;

#define DIM 100
bool A[DIM][DIM];
short viz[DIM];
int n, nr_comp;

ifstream in("date.txt");

void DFS(int x) {
    viz[x]=nr_comp;

    for (int i=0; i<n; i++)
        if (A[x][i] && !viz[i])
            DFS(i);
}

bool conex() {
    nr_comp = 1;
    DFS(0);

    for (int i=0; i<n; i++)
        if (viz[i] != nr_comp)
            return false;

    return true;
}

void componente_conexe() {
    nr_comp = 0;
    for (int i=0; i<n; i++)
        if (viz[i]==0) {
            nr_comp++;
            DFS(i);
        }
}

void afisare_componente_conexe() {
    for (int i=1; i<=nr_comp; i++) {
        cout<<"Componenta "<<i<<": ";
        for (int j=0; j<n; j++)
            if (viz[j] == i)
                cout<<j+1<<" ";
        cout<<endl;
    }
}

void resetare_vizite() {
    for (int i=0; i<n; i++)
        viz[i] = false;
}

int main()
{
    in>>n;

    for (int i=0; i<n; i++)
        for (int j=0; j<n; j++)
            in>>A[i][j];

    if (!conex()) {
        cout<<"Graful nu este conex."<<endl;
        resetare_vizite();
        componente_conexe();
        afisare_componente_conexe();
    } else {
        cout<<"Graful este conex."<<endl;
    }

    return 0;
}
