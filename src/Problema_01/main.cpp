/*
 * Realizati un program cu grafuri neorientate in care se citeste dintr-un
 * fisier pe cate o linie separat numarul de noduri si perechile de noduri
 * intre care exista o muchie (lista de muchii); construiti si afisati pe
 * ecran matricea de adiacenta.
 */

#include <iostream>
#include <fstream>
#include <vector>

using namespace std;

ifstream in("date.txt");

int** matrice_adiacenta(int nr_noduri, vector<int> muchii) {
    int** matrice = new int*[nr_noduri];

    for (int i=0; i<nr_noduri; i++) {
        matrice[i] = new int[nr_noduri];
        for (int j=0; j<nr_noduri; j++)
            matrice[i][j] = 0;
    }

    for (int i=0; i<muchii.size(); i+=2) {
        int a = muchii[i] - 1;
        int b = muchii[i+1] - 1;

        matrice[a][b] = matrice[b][a] = 1;
    }

    return matrice;
}

void afisare_matrice(int lin, int col, int **matrice) {
    for (int i=0; i<lin; i++) {
        for (int j=0; j<col; j++)
            cout<<matrice[i][j]<<" ";
        cout<<endl;
    }
}

int main()
{
    int nr_noduri, a, b;
    vector<int> muchii;
    in>>nr_noduri;

    while (in>>a>>b) {
        muchii.push_back(a);
        muchii.push_back(b);
    }

    int** matrice = matrice_adiacenta(nr_noduri, muchii);
    afisare_matrice(nr_noduri, nr_noduri, matrice);

    return 0;
}
