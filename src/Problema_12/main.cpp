/*
 * Scrieti un program care apeleaza doua functii, si anume cele prin care se
 * parcurge graful in latime si apoi in adancime.
 */

#include <iostream>
#include <fstream>

using namespace std;

#define DIM 100
bool A[DIM][DIM];
short b[DIM], c[DIM];
int n;
ifstream in("date.txt");

void citire() {
    in>>n;

    for (int i=0; i<n; i++)
    for (int j=0; j<n; j++) {
        in>>A[i][j];
    }
}

void BFS(int s) {
    int st=0, dr=0;
    c[s] = true;
    b[0] = s;
    while (st <= dr) {
        for (int i=0; i<n; i++)
            if (A[b[st]][i] && !c[i]) {
                dr++;
                b[dr]=i;
                c[i]=1;
            }

        cout<<b[st]+1<<" ";
        st++;
    }

    cout<<endl;
}

void DFS(int s) {
    int i;
    b[s]=true;
    for (i=1; i<=n; i++) {
        if (A[s][i] && !b[i])
            DFS(i);
    }

    cout<<s+1<<" ";
}

int main()
{
    citire();

    cout<<"Depth first:   ";
    DFS(0);
    cout<<endl;

    cout<<"Breadth first: ";
    BFS(0);
    cout<<endl;

    return 0;
}
