/*
 * Scrieti un program care sa verifice daca un graf este complet.
 */
#include <iostream>
#include <fstream>

using namespace std;

fstream in("date.txt");

int main()
{
    bool val;
    int n, muchii=0;
    in>>n;

    for (int i=0; i<n; i++)
        for (int j=0; j<n; j++) {
            in>>val;

            if (val)
                muchii++;
        }

    muchii /= 2;

    if (muchii == n * (n-1)/2)
        cout<<"Graful este complet."<<endl;
    else
        cout<<"Graful nu este complet."<<endl;

    return 0;
}
